package com.example.redmadrobot.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.redmadrobot.R;
import com.example.redmadrobot.model.Photo;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class PhotoItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_item);

        ImageView mainPhoto = findViewById(R.id.mainPhoto);
        TextView description = findViewById(R.id.descriptionPhoto);
        TextView size = findViewById(R.id.sizePhoto);
        TextView urlPhoto = findViewById(R.id.urlPhoto);

        Intent intent = getIntent();
        if(intent != null){
            description.setText(intent.getStringExtra("description"));
            size.setText(intent.getStringExtra("size"));
            SpannableString ss = new SpannableString(intent.getStringExtra("fullurl"));
            String linkedText = String.format("<a href=\"%s\">Open in browser</a> ", intent.getStringExtra("fullurl"));
            urlPhoto.setText(Html.fromHtml(linkedText));
            urlPhoto.setMovementMethod(LinkMovementMethod.getInstance());
            Picasso.get().load(intent.getStringExtra("smallurl")).fit().centerInside().placeholder(R.drawable.loading).into(mainPhoto);
        }
    }
}
