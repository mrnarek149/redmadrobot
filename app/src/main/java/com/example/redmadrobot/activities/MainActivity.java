package com.example.redmadrobot.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SearchView;

import com.example.redmadrobot.R;
import com.example.redmadrobot.fragments.CollectionFragment;
import com.example.redmadrobot.fragments.MainFragment;
import com.example.redmadrobot.fragments.SearchFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, SearchView.OnQueryTextListener {

    ImageButton mainButton;
    ImageButton collectionButton;
    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainButton = findViewById(R.id.mainButton);
        collectionButton = findViewById(R.id.collectionButton);

        mainButton.setOnClickListener(this);
        collectionButton.setOnClickListener(this);

        ft = getSupportFragmentManager().beginTransaction();
        MainFragment mainFragment = new MainFragment();
        ft.replace(R.id.fragment, mainFragment);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.searchable, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public void onClick(View v) {
        ft = getSupportFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.mainButton: {
                MainFragment mainFragment = new MainFragment();
                ft.replace(R.id.fragment, mainFragment);
                break;
            }
            case R.id.collectionButton: {
                CollectionFragment collectionFragment = new CollectionFragment();
                ft.replace(R.id.fragment, collectionFragment);
                break;
            }
        }
        ft.commit();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Bundle bundle = new Bundle();
        bundle.putString("search", query);
        ft = getSupportFragmentManager().beginTransaction();
        SearchFragment searchFragment = new SearchFragment();
        searchFragment.setArguments(bundle);
        ft.replace(R.id.fragment, searchFragment);
        ft.commit();
        getWindow().getCurrentFocus().clearFocus();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}