package com.example.redmadrobot.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.redmadrobot.R;
import com.example.redmadrobot.data.PhotoAdapter;
import com.example.redmadrobot.model.Photo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PhotoCollectionActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PhotoAdapter photoAdapter;
    private ArrayList<Photo> photos;
    private RequestQueue requestQueue;
    private Context context;
    private LinearLayoutManager llm;
    private boolean isLastPage = false;
    private boolean loading = false;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);
        recyclerView = findViewById(R.id.searchRecycler);
        llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        context = this;
        photos = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(this);

        photoAdapter = new PhotoAdapter(context, photos);
        recyclerView.setAdapter(photoAdapter);

        getPhotos(getIntent().getStringExtra("id"));
    }

    private int startSize = 1;

    private void getPhotos(final String id) {
        String url = "https://api.unsplash.com/collections/" + id + "/photos?client_id=a4bceecb863aef5f85e47d4052dad50f7c30fe89a7117526fec537e8f04c0d05&Wwob8kwOCk&page=" + page;
        Toast.makeText(context, String.valueOf(page), Toast.LENGTH_SHORT).show();
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    if (response.isNull(0)) isLastPage = true;
                    else {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            String description = jsonObject.getString("alt_description");
                            if (description.equals("null")) description = "no description";
                            String size = jsonObject.getString("width") + " * " + jsonObject.getString("height");
                            JSONObject urls = jsonObject.getJSONObject("urls");
                            String fullUrl = urls.getString("full");
                            String smallUrl = urls.getString("small");

                            Photo photo = new Photo();
                            photo.setDescription("Description: " + description);
                            photo.setSize("Size: " + size);
                            photo.setFullurl(fullUrl);
                            photo.setSmallurl(smallUrl);

                            photos.add(photo);
                        }
                        startSize = photos.size() - startSize;
                        photoAdapter.notifyItemRangeChanged(startSize, photos.size() - startSize);
                    }
                    loading = false;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(request);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int lastvisibileitem = llm.findLastVisibleItemPosition();
                if (lastvisibileitem == photoAdapter.getItemCount() - 1) {
                    if (!isLastPage && !loading) {
                        loading = true;
                        page++;
                        getPhotos(id);
                    }
                }
            }
        });
    }

}
