package com.example.redmadrobot.data;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.redmadrobot.R;
import com.example.redmadrobot.activities.PhotoItemActivity;
import com.example.redmadrobot.model.Photo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>{

    private Context context;
    private ArrayList<Photo> photos;

    public PhotoAdapter(Context context, ArrayList<Photo> photos){
        this.context = context;
        this.photos = photos;
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.photo_list, parent, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder photoViewHolder, int position) {
        Photo currentPhoto = photos.get(position);
        String smallUrl = currentPhoto.getSmallurl();
        Picasso.get().load(smallUrl).fit().centerInside().placeholder(R.drawable.loading).into(photoViewHolder.mainPhoto);
    }


    @Override
    public int getItemCount() {
        return photos.size();
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mainPhoto;

        public PhotoViewHolder(@NonNull View itemView) {
            super(itemView);
            mainPhoto = itemView.findViewById(R.id.mainPhoto);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            Photo photo = photos.get(getAdapterPosition());
            Intent intent = new Intent(context, PhotoItemActivity.class);
            intent.putExtra("description", photo.getDescription());
            intent.putExtra("smallurl", photo.getSmallurl());
            intent.putExtra("fullurl", photo.getFullurl());
            intent.putExtra("size", photo.getSize());
            context.startActivity(intent);
        }
    }
}
