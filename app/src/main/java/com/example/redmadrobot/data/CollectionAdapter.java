package com.example.redmadrobot.data;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.redmadrobot.R;
import com.example.redmadrobot.activities.PhotoCollectionActivity;
import com.example.redmadrobot.model.Collection;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CollectionAdapter extends RecyclerView.Adapter<CollectionAdapter.CollectionViewHolder> {

    private Context context;
    private ArrayList<Collection> collections;

    public CollectionAdapter(Context context, ArrayList<Collection> collections) {
        this.context = context;
        this.collections = collections;
    }

    @NonNull
    @Override
    public CollectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.collection_list, parent, false);
        return new CollectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CollectionViewHolder collectionViewHolder, int position) {
        Collection currentCollection = collections.get(position);

        String title = currentCollection.getTitle();
        String urlCollection = currentCollection.getUrl();

        collectionViewHolder.title.setText(title);
        Picasso.get().load(urlCollection).fit().centerInside().placeholder(R.drawable.loading).into(collectionViewHolder.photoCollection);
    }

    @Override
    public int getItemCount() {
        return collections.size();
    }

    public class CollectionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView photoCollection;
        TextView title;

        public CollectionViewHolder(@NonNull View itemView) {
            super(itemView);
            photoCollection = itemView.findViewById(R.id.photoCollection);
            title = itemView.findViewById(R.id.titleCollection);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, PhotoCollectionActivity.class);
            intent.putExtra("id", collections.get(getAdapterPosition()).getId());
            context.startActivity(intent);
        }
    }
}
