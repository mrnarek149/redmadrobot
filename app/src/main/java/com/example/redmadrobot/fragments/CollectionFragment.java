package com.example.redmadrobot.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.redmadrobot.R;
import com.example.redmadrobot.data.CollectionAdapter;
import com.example.redmadrobot.model.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CollectionFragment extends Fragment {


    public CollectionFragment() {
        // Required empty public constructor
    }

    private RecyclerView recyclerView;
    private CollectionAdapter collectionAdapter;
    private ArrayList<Collection> collections;
    private RequestQueue requestQueue;
    private LinearLayoutManager llm;
    private boolean isLastPage = false;
    private boolean loading = false;
    private int page = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.collection, null);

        recyclerView = v.findViewById(R.id.recyclerCollection);
        llm = new LinearLayoutManager(v.getContext());
        recyclerView.setLayoutManager(llm);
        
        collections = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(v.getContext());
        collectionAdapter = new CollectionAdapter(getContext(), collections);
        recyclerView.setAdapter(collectionAdapter);

        getCollections();

        return v;
    }

    private int startSize = 1;

    private void getCollections() {

        String url = "https://api.unsplash.com/collections/?client_id=a4bceecb863aef5f85e47d4052dad50f7c30fe89a7117526fec537e8f04c0d05&Wwob8kwOCk&page=" + page;
        Toast.makeText(getContext(), String.valueOf(page), Toast.LENGTH_SHORT).show();
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    if (response.isNull(0)) isLastPage = true;
                    else {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonObject = response.getJSONObject(i);
                            String id = jsonObject.getString("id");
                            String title = jsonObject.getString("title");
                            JSONObject photo = jsonObject.getJSONArray("preview_photos").getJSONObject(0);
                            String url = photo.getJSONObject("urls").getString("small");

                            Collection collection = new Collection();
                            collection.setId(id);
                            collection.setTitle(title);
                            collection.setUrl(url);

                            collections.add(collection);
                        }
                        startSize = collections.size() - startSize;
                        collectionAdapter.notifyItemRangeChanged(startSize, collections.size() - startSize);
                    }
                    loading = false;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(request);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int lastvisibileitem = llm.findLastVisibleItemPosition();

                if (lastvisibileitem == collectionAdapter.getItemCount() - 1) {
                    if (!isLastPage && !loading) {
                        loading = true;
                        page++;
                        getCollections();
                    }
                }
            }
        });
    }

}
