package com.example.redmadrobot.fragments;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.redmadrobot.R;
import com.example.redmadrobot.activities.PhotoItemActivity;
import com.example.redmadrobot.data.PhotoAdapter;
import com.example.redmadrobot.model.Photo;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener {


    public MainFragment() {
        // Required empty public constructor
    }

    private RecyclerView recyclerView;
    private PhotoAdapter photoAdapter;
    private ArrayList<Photo> photos;
    private RequestQueue requestQueue;

    private ImageView photoDay;
    private Photo photo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main, null);

        recyclerView = view.findViewById(R.id.recyclerMain);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        photos = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(view.getContext());

        photo = new Photo();
        photoDay = view.findViewById(R.id.photoOfTheDay);
        photoDay.setOnClickListener(this);

        getDayPhoto();
        getPhotos();
        return view;
    }

    private void getPhotos() {
        String url = "https://api.unsplash.com/photos/random/?client_id=a4bceecb863aef5f85e47d4052dad50f7c30fe89a7117526fec537e8f04c0d05&Wwob8kwOCk&count=10";

        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject jsonObject = response.getJSONObject(i);
                        String description = jsonObject.getString("alt_description");
                        if (description.equals("null")) description = "no description";
                        String size = jsonObject.getString("width") + " * " + jsonObject.getString("height");
                        JSONObject urls = jsonObject.getJSONObject("urls");
                        String fullUrl = urls.getString("full");
                        String smallUrl = urls.getString("small");

                        Photo photo = new Photo();
                        photo.setDescription("Description: " + description);
                        photo.setSize("Size: " + size);
                        photo.setFullurl(fullUrl);
                        photo.setSmallurl(smallUrl);

                        photos.add(photo);
                    }
                    photoAdapter = new PhotoAdapter(getContext(), photos);
                    recyclerView.setAdapter(photoAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(request);
    }

    private void getDayPhoto() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Document doc = Jsoup.connect("https://unsplash.com").get();
                    Element href = doc.getElementsByClass("_2Mc8_ ICezk").first();
                    String urlPhoto = href.attr("href");

                    String url = "https://api.unsplash.com/" + urlPhoto + "?client_id=a4bceecb863aef5f85e47d4052dad50f7c30fe89a7117526fec537e8f04c0d05&Wwob8kwOCk";
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String description = response.getString("alt_description");
                                if (description.equals("null")) description = "no description";
                                String size = response.getString("width") + " * " + response.getString("height");
                                JSONObject urls = response.getJSONObject("urls");
                                String fullUrl = urls.getString("full");
                                String smallUrl = urls.getString("small");

                                photo.setDescription("Description: " + description);
                                photo.setSize("Size: " + size);
                                photo.setFullurl(fullUrl);
                                photo.setSmallurl(smallUrl);
                                Picasso.get().load(smallUrl).fit().centerInside().placeholder(R.drawable.loading).into(photoDay);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });
                    requestQueue.add(request);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photoOfTheDay: {
                Intent intent = new Intent(getContext(), PhotoItemActivity.class);
                intent.putExtra("description", photo.getDescription());
                intent.putExtra("smallurl", photo.getSmallurl());
                intent.putExtra("fullurl", photo.getFullurl());
                intent.putExtra("size", photo.getSize());
                getContext().startActivity(intent);
                break;
            }
        }
    }
}
