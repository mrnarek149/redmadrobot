package com.example.redmadrobot.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.redmadrobot.R;
import com.example.redmadrobot.data.PhotoAdapter;
import com.example.redmadrobot.model.Photo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {


    public SearchFragment() {
        // Required empty public constructor
    }

    private RecyclerView recyclerView;
    private PhotoAdapter photoAdapter;
    private ArrayList<Photo> photos;
    private RequestQueue requestQueue;
    private LinearLayoutManager llm;
    private boolean isLastPage = false;
    private boolean loading = false;
    private int page = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search, null);
        recyclerView = view.findViewById(R.id.searchRecycler);
        llm = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(llm);
        photos = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(view.getContext());

        photoAdapter = new PhotoAdapter(getContext(), photos);
        recyclerView.setAdapter(photoAdapter);

        getPhotos(getArguments().getString("search"));

        return view;
    }

    private int startSize = 1;

    private void getPhotos(final String search) {
        String url = "https://api.unsplash.com/search/photos/?client_id=a4bceecb863aef5f85e47d4052dad50f7c30fe89a7117526fec537e8f04c0d05&Wwob8kwOCk&query=" + search + "&page=" + page;
        Toast.makeText(getContext(), String.valueOf(page), Toast.LENGTH_SHORT).show();
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("results");

                    if (jsonArray.isNull(0)) isLastPage = true;
                    else {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String description = jsonObject.getString("alt_description");
                            if (description.equals("null")) description = "no description";
                            String size = jsonObject.getString("width") + " * " + jsonObject.getString("height");
                            JSONObject urls = jsonObject.getJSONObject("urls");
                            String fullUrl = urls.getString("full");
                            String smallUrl = urls.getString("small");

                            Photo photo = new Photo();
                            photo.setDescription("Description: " + description);
                            photo.setSize("Size: " + size);
                            photo.setFullurl(fullUrl);
                            photo.setSmallurl(smallUrl);

                            photos.add(photo);
                        }
                        startSize = photos.size() - startSize;
                        photoAdapter.notifyItemRangeChanged(startSize, photos.size() - startSize);
                    }
                    loading = false;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        requestQueue.add(request);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int lastvisibileitem = llm.findLastVisibleItemPosition();
                if (lastvisibileitem == photoAdapter.getItemCount() - 1) {
                    if (!isLastPage && !loading) {
                        loading = true;
                        page++;
                        getPhotos(search);
                    }
                }
            }
        });
    }

}
