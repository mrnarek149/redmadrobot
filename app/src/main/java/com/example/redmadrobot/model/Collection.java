package com.example.redmadrobot.model;

public class Collection {

    private String id;
    private String title;
    private String url;

    public Collection() {}

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
